/* global _ */
/* eslint-disable no-param-reassign */
import * as utils from 'https://unpkg.com/@oursci/dashboards@1/utils.js';

/**
 * Copies all values from data[sourceField] to data[destField]
 * where data[sourceField] is an Array
 *
 * @param {Object} data
 * @param {string} sourceField
 * @param {string} destField
 */
export function remapField(data, sourceField, destField) {
  if (!data[sourceField]) {
    console.error(`source field does not exist: ${sourceField}`);
    console.log(data);
    _.forEach(data, (v, k) => {
      if (k.split('.').slice(-1)[0] === sourceField) {
        console.log(`did you mean: ${k}`);
      }
    });
    console.log();
  }

  data[destField] = data[sourceField];
  data[sourceField] = new Array(data[sourceField].length);
}

/**
 * samples as {remapField} with config containing key / value
 * pairs to map fixing
 *
 * @param {object} data
 * @param {object} config
 */
export function remapFields(data, config) {
  _.forEach(config, (v, k) => {
    remapField(data, k, v);
  });
}

/**
 * Returns counts for distinct input
 *
 * @param {object} data The data set
 * @param {string} field The field in the data set
 * @param {object} countConfig The config specifying which values are counted as what
 *
 * example:
 * const states = {
 *   CT: ['connecticut', 'CT'],
 *   NY: ['new york', 'NY'],
 *   MI: ['michigan', 'MI'],
 *   PA: ['pennsylvania', 'PA'],
 *   MA: ['ma'],
 * };
 */
export function countValues(data, field, countConfig) {
  const stats = _.clone(countConfig);

  _.forEach(countConfig, (v, k) => {
    stats[k] = 0;
  });

  _.forEach(data[field], (state) => {
    // console.log(`state: ${state}`);
    let found = false;
    _.forEach(countConfig, (arr, short) => {
      if (_.find(arr, s => s === state.toLowerCase())) {
        stats[short] += 1;
        found = true;
      }
    });

    if (!found) {
      if (!stats[state]) {
        stats[state] = 0;
      }
      stats[state] += 1;
    }
  });
  return stats;
}

/**
 * Strips all fields away that are not present in {fildsToKeep}
 *
 * @param {object} data
 * @param {array} fieldsToKeep
 */
export function removeFields(data, fieldsToKeep) {
  _.forEach(_.keys(data), (k) => {
    if (!_.includes(fieldsToKeep, k)) {
      delete data[k];
    }
  });
}

/**
 * remaps wavelengths
 * @param {object} data
 */
export function remapWavelengths(data, wavelengthRemap) {
  const keyMap = {};

  _.forEach(data, (v, k) => {
    _.forEach(wavelengthRemap, (wv, wk) => {
      if (k.endsWith(`_${wk}`)) {
        const newKey = `${k.split(`_${wk}`)[0]}_${wv}`;
        keyMap[k] = newKey;
      }
    });
  });

  _.forEach(keyMap, (v, k) => {
    data[v] = data[k];
  });

  _.forEach(keyMap, (v, k) => {
    delete data[k];
  });
}

/**
 * @callback violationCallback
 * @param {object} data the data table
 * @param {Number} index the current index
 */

/**
 *
 * This function cleans up data by removing outliers
 *
 * @returns returns the summary of the removal
 *
 * @param {object} data The data table
 * @param {string} field the field in the table
 * @param {Number} min the minimum value that is allowed
 * @param {Number} max the maximum value that is allowed
 * @param {violationCallback} violationCallback the callback executed if a rule is violated
 */
export function removeOutliers(data, field, min, max, violationCallback) {
  if (!data[field]) {
    console.error(`field does not exist: ${field}`);
  }

  const res = {
    minCount: 0,
    maxCount: 0,
    nan: 0,
    integrity: 100,
    integrityStr: '100%',
  };

  _.forEach(data[field], (iv, idx) => {
    const v = _.toNumber(iv);

    if (_.isNaN(v)) {
      if (!_.isNumber(iv)) {
        res.nan += 1;
        return;
      }
    }

    if (min && v < min) {
      if (_.isFunction(violationCallback)) {
        violationCallback(data, idx);
      }

      data[field][idx] = undefined;
      res.minCount += 1;
    }

    if (max && v > max) {
      if (_.isFunction(violationCallback)) {
        violationCallback(data, idx);
      }

      data[field][idx] = undefined;
      res.maxCount += 1;
    }
  });

  res.integrity = (1 - (res.minCount + res.maxCount) / data[field].length) * 100;
  res.integrityStr = `${_.floor(res.integrity)} %`;

  return res;
}

/**
 * Makes Number or Undefined
 *
 * @param {object} data the data table
 * @param {string} key the key of the data to be sanetized
 */
export function sanetizeNumber(data, ...keys) {
  const res = {
    count: 0,
    nan: 0,
    completeness: 100,
    completenessStr: '100%',
  };

  _.forEach(keys, (key) => {
    _.forEach(data[key], (v, idx) => {
      res.count += 1;
      if (!v) {
        data[key][idx] = undefined;
        res.nan += 1;
        return;
      }

      const n = _.toNumber(v);
      if (_.isNaN(n)) {
        data[key][idx] = undefined;
        res.nan += 1;
      } else {
        data[key][idx] = n;
      }
    });
  });

  res.completeness = (1 - res.nan / res.count) * 100;
  res.completenessStr = `${_.floor(res.completeness)} %`;

  return res;
}

/**
 *
 * @param {object} data
 * @param {string} target of the new column
 * @param  {...any} keys for instance carrot_scan_1.data.median_365 and carrot_scan_2.data.median_365
 */
export function averageColums(data, target, ...keys) {
  sanetizeNumber(data, ...keys);
  const cols = _.map(keys, (k) => {
    if (!data[k]) {
      console.error(`key does not exist in data: ${k}`);
      console.log(data);
    }

    return data[k];
  });
  data[target] = new Array(cols[0].length);

  _.forEach(cols[0], (v, idx) => {
    let sum = 0;
    let divider = 0;
    for (let i = 0; i < cols.length; i += 1) {
      const val = cols[i][idx];
      if (val) {
        sum += val;
        divider += 1;
      }
    }

    if (divider === 0) {
      data[target][idx] = undefined;
    } else {
      data[target][idx] = sum / divider;
    }
  });
}

/**
 * NOT TESTED! DON'T JUST USE
 * returns median values over data columns
 *
 * @param {object} data
 * @param  {...any} keys keys that should be used as median input
 */
export function median(data, ...keys) {
  const res = {};
  _.forEach(keys, (k) => {
    if (!data[k]) {
      console.error(`key does not exist in data: ${k}`);
      console.log(data);
      return;
    }
    let count = 0;

    const val = _.reduce(
      data[k],
      (sum, n) => {
        if (!n) {
          return sum;
        }
        const v = _.toNumber(n);
        if (_.isNaN(v)) {
          return sum;
        }

        count += 1;
        return sum + n;
      },
      0,
    );

    res[k] = val / count;
  });

  return res;
}

/**
 * @returns an array of values according to elements
 * @param {object} data the data set
 * @param {string} fieldroot root ahed of .xrf.data.elements
 * @param {array} minerals list of minerals to search for
 */
export function extractXRF(data, fieldroot, minerals) {
  const ret = {};
  _.forEach(minerals, (m) => {
    ret[m] = new Array(data.metaInstanceID.length);
    _.fill(ret[m], undefined);
  });

  _.forEach(data[`${fieldroot}.xrf.data.elements`], (item, index) => {
    if (item) {
      const elements = item.split(',');
      //      console.log(elements);
      _.forEach(elements, (element, elementIndex) => {
        const numericValues = data[`${fieldroot}.xrf.data.elements_ppm`][index];
        if (!numericValues) {
          return;
        }

        const splitted = numericValues.split(',');
        const e = splitted[elementIndex];
        if (!e) {
          return;
        }
        const val = _.toNumber(e);

        if (!_.includes(minerals, element.trim())) {
          console.error(`element unknown: ${element.trim()}`);
          return;
        }

        ret[element][index] = val;
      });
    }
  });

  return ret;
}

/**
 * This function collates data of the same sampleID into a new row
 * taking data from each of the most recent row whenever there is data available
 *
 * example
 *
 *
 * time             id  proteins  color
 * --------------------------------------
 * 2018-11-14 12:00 A6  500____   blue___
 * 2018-11-14 14:00 A6  _______   _______
 * 2018-11-16 12:00 A6  200____   _______
 * 2018-11-18 12:00 A6  _______   yellow_
 * 2018-11-18 13:00 C9  400____   green__
 * ======================================
 * 2018-11-18 12:00 A6  200       yellow
 * 2018-11-18 13:00 C9  400       green
 *
 * @param {object} data the survey
 * @param {string} the time field
 * @param {string} sampleIdField the ID on which to collate on
 */
export function collateNewestOnId(data, sampleIdField, timeField) {
  // group by distinct ids
  // sort by date
  // create result row
  // override column value if newer but not empty
  // transform back into survey format

  const res = [];
  const byRow = utils.zip(data);

  const byRowSorted = _.sortBy(byRow, timeField);

  const idMap = {};

  _.forEach(byRowSorted, (row, idx) => {
    if (!_.has(idMap, row[sampleIdField])) {
      idMap[row[sampleIdField]] = [idx];
    } else {
      idMap[row[sampleIdField]].push(idx);
    }
  });

  console.log(byRowSorted);
  console.log(idMap);
  console.log(_.filter(byRowSorted, v => v[sampleIdField] === '255'));

  _.forEach(idMap, (indices) => {
    // rows with same id
    const base = byRowSorted[indices[0]];
    _.forEach(indices, (idx) => {
      // for each row
      const row = byRowSorted[idx];
      _.forEach(row, (value, columnName) => {
        if (value) {
          base[columnName] = value;
        }
      });
    });
    res.push(base);
  });

  const resultSet = {};
  _.forEach(data, (v, columnName) => {
    _.forEach(res, (entry) => {
      if (!_.has(resultSet, columnName)) {
        resultSet[columnName] = [];
      }
      resultSet[columnName].push(entry[columnName]);
    });
  });

  console.log(resultSet);
  return resultSet;

  // const grouped = utils.groupBy(data, sampleIdField);
  // console.log(grouped);
}
