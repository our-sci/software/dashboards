/* global _,moment, d3 */
import * as utils from 'https://unpkg.com/@oursci/dashboards@1/utils.js';

const margin = {
  l: 30,
  r: 30,
  b: 50,
  t: 50,
};

const marginWithLabels = {
  b: 50,
  t: 50,
};

export function stateMap(title, stateSampleCount, hoverTexts, scale, center, color = 'blue') {
  const colorInterpolator = d3.interpolateRgb('white', color);

  const colorscale = _.map([0, 0.2, 0.4, 0.6, 0.8, 1], v => [v, colorInterpolator(v)]);

  const data = [
    {
      type: 'choropleth',
      locationmode: 'USA-states',
      locations: _.keys(stateSampleCount),
      z: _.values(stateSampleCount),
      text: hoverTexts,
      zmin: 0,
      zmax: Math.max(..._.values(stateSampleCount)),
      colorscale,
      colorbar: {
        title: '# Surveyed',
        thickness: 15,
      },
      marker: {
        line: {
          color: 'rgb(255,255,255)',
          width: 2,
        },
      },
    },
  ];

  const layout = {
    title,
    geo: {
      scope: 'usa',
      showlakes: true,
      lakecolor: 'rgb(255,255,255)',
      projection: {
        scale,
      },
      center,
    },
    margin,
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function barchart(dataMap, labels) {
  const data = [
    {
      type: 'bar',
      x: _.keys(dataMap),
      y: _.values(dataMap),
      text: _.map(_.keys(dataMap), l => labels[l]),
    },
  ];

  const layout = {
    yaxis: {
      autorange: true,
      type: 'linear',
    },
    margin,
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function multiBarchart(x, values) {
  const data = [];
  _.forEach(values, (y, name) => {
    // console.log(name);
    data.push({
      x,
      y,
      name,
      type: 'bar',
    });
  });

  const layout = {
    barmode: 'group',
    margin,
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function heatmap({
  formDef = {}, survey = {}, colX = '', colY = '', color = 'blue',
} = {}) {
  const colorInterpolator = d3.interpolateRgb('white', color);

  const colorscale = _.map([0, 0.2, 0.4, 0.6, 0.8, 1], v => [v, colorInterpolator(v)]);

  const optionItems = name => _.find(formDef.items, i => i.id === name).items;

  const xItems = _.map(optionItems(colX), i => i.value);
  const yItems = _.map(optionItems(colY), i => i.value);

  console.log('items');
  console.log(xItems);
  console.log(yItems);

  const itemMap = [];
  _.forEach(xItems, () => itemMap.push(new Array(yItems.length)));

  _.forEach(survey[colX], (v, idx) => {
    const xIdx = _.indexOf(xItems, v);
    const yIdx = _.indexOf(yItems, survey[colY][idx]);

    if (xIdx < 0 || yIdx < 0) {
      return;
    }

    if (!itemMap[xIdx][yIdx]) {
      itemMap[xIdx][yIdx] = 1;
    } else {
      itemMap[xIdx][yIdx] += 1;
    }
  });

  console.log(itemMap);
  const data = [
    {
      z: itemMap,
      x: _.map(optionItems(colX), i => i.label),
      y: _.map(optionItems(colY), i => i.label),
      type: 'heatmap',
      colorscale,
    },
  ];

  const layout = {
    margin: {
      l: 50,
      r: 30,
      b: 50,
      t: 50,
    },
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function activityGraph(surveys, measurements) {
  const start = moment()
    .subtract(14, 'days')
    .format('YYYY-MM-DD');
  const stop = moment().format('YYYY-MM-DD');

  const data = [
    {
      type: 'bar',
      name: 'Survey taken',
      x: _.keys(surveys),
      y: _.values(surveys),
      line: { color: '#17BECF' },
    },
    {
      type: 'bar',
      name: 'Measurements taken',
      x: _.keys(measurements),
      y: _.values(measurements),
      line: { color: '#17BECF' },
    },
  ];

  const layout = {
    xaxis: {
      range: [start, stop],
      rangeselector: {
        buttons: [
          {
            count: 14,
            label: '2w',
            step: 'day',
            stepmode: 'backward',
          },
          {
            count: 1,
            label: '1m',
            step: 'month',
            stepmode: 'backward',
          },
          {
            count: 6,
            label: '6m',
            step: 'month',
            stepmode: 'backward',
          },
          { step: 'all' },
        ],
      },
      rangeslider: { range: ['2018-06-01', stop] },
      type: 'date',
      tickformat: '%a %e %b',
    },
    yaxis: {
      autorange: true,
      type: 'linear',
    },
    margin,
    showlegend: true,
    legend: {
      x: 0,
      y: 1,
      bordercolor: '#000',
    },
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function histogram(values, bins) {
  const data = [
    {
      x: values,
      type: 'histogram',
      nbinsx: bins,
    },
  ];

  const layout = {
    bargap: 0.1,
    margin,
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function scatter(x, y, axis) {
  const data = [
    {
      x,
      y,
      mode: 'markers',
      type: 'scatter',
    },
  ];

  const layout = {
    xaxis: {
      title: axis[0],
    },
    yaxis: {
      title: axis[1],
    },
    marginWithLabels,
  };

  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function linecharts(x, values, names, axis) {
  const data = _.map(values, (v, idx) => ({
    x,
    y: v,
    name: names[idx],
    type: 'scatter',
    connectgaps: true,
    mode: 'lines',
    line: {
      width: 0.2,
      color: '#3390E6',
      smoothing: 1,
      shape: 'spline',
      simplify: true,
    },
  }));

  const layout = {
    xaxis: {
      title: axis[0],
    },
    yaxis: {
      title: axis[1],
    },
    marginWithLabels,
  };
  return {
    data,
    layout,
    config: {
      showLinks: false,
      responsive: true,
    },
  };
}

export function countNumbers(column) {
  let totalN = 0;
  _.forEach(column, (v) => {
    if (!v) {
      return;
    }
    totalN += 1;
  });
  return totalN;
}

export function hasAtLeastOneNumber(fullSurvey, keys) {
  return utils.filter(fullSurvey, (item) => {
    let valid = false;
    _.forEach(keys, (value) => {
      const v = item[value];
      if (v === undefined) {
        return;
      }
      if (Number.isNaN(item[value])) {
        return;
      }
      valid = true;
    });
    return valid;
  });
}

export function numberFilter(field) {
  return (item) => {
    const v = item[field];
    if (v === undefined) {
      // || v.trim() === '') {
      return false;
    }
    if (Number.isNaN(v)) {
      return false;
    }
    return true;
  };
}

export function mean(array, field) {
  return _.mean(array[field].map(n => Number.parseFloat(n)));
}

export function getSharedN(array1, array2) {
  let totalN = 0;
  for (let i = 0; i < array1.length; i++) {
    //    console.log(Number(array1[i]), Number(array2[i]));
    if (
      array1[i] !== ''
      && !Number.isNaN(Number(array1[i]))
      && array2[i] !== ''
      && !Number.isNaN(Number(array2[i]))
    ) {
      totalN += 1;
    }
  }
  //    console.log(totalN);
  return totalN;
}

export function hasAllValuesAsNumber(fullSurvey, requiredIndexes) {
  return utils.filter(fullSurvey, (item) => {
    let valid = true;
    _.forEach(requiredIndexes, (key) => {
      const v = item[key];
      if (v === undefined) {
        // || v.trim() === '') {
        valid = false;
      }
      if (Number.isNaN(v)) {
        valid = false;
      }
    });
    return valid;
  });
}

export function tracesFromKeys(validCarrots, carrotscan2Keys, nameField = '') {
  const traces = [];
  const names = [];
  _.map(utils.zip(validCarrots), (item) => {
    const yarr = _.map(carrotscan2Keys, (key) => {
      const v = item[key];
      if (v === undefined) {
        // || v.trim() === '') {
        return undefined;
      }
      if (Number.isNaN(v)) {
        return undefined;
      }
      return Number.parseFloat(v);
    });

    if (nameField) {
      names.push(item[nameField]);
    }
    traces.push(yarr);
  });

  return {
    traces,
    names,
  };
}

export function surveyActivity(joined, newSubmissionDays) {
  const ref = moment().subtract(newSubmissionDays, 'days');
  const uniqTimes = _.uniq(joined.metaDateModified);
  const newSubmissions = _.filter(uniqTimes, str => moment(str).isAfter(ref)).length;
  const activity = _.assign(
    {},
    ..._.map(
      _.groupBy(uniqTimes, t => moment(t)
        .startOf('day')
        .format('YYYY-MM-DD')),
      (g, d) => {
        const res = {};
        res[d] = g.length;
        return res;
      },
    ),
  );

  const measurementTimes = _.chain(joined)
    .flatMap((data, key) => {
      if (key.endsWith('meta.date')) {
        return _.filter(data, v => !!v);
      }
      return [];
    })
    .uniq()
    .value();
  const measurementActivity = _.assign(
    {},
    ..._.map(
      _.groupBy(measurementTimes, t => moment(t)
        .startOf('day')
        .format('YYYY-MM-DD')),
      (g, d) => {
        const res = {};
        res[d] = g.length;
        return res;
      },
    ),
  );

  return {
    activity,
    measurementActivity,
    newSubmissions,
  };
}

const waveClass = {
  median: 'median_',
  stddev: 'three_stdev_',
  spad: 'spad_',
};

/**
 * outputs the prefix if it is a wavelength
 * @param {string} key column name
 */
function classifyAndGroupWavelengths(key) {
  const r = {};

  _.forEach(waveClass, (prefix, cls) => {
    const expr = `\\.data\\.${prefix}[0-9]*$`;
    const reg = new RegExp(expr, 'g');
    const matches = key.match(reg);
    if (matches) {
      const idx = key.indexOf(matches[0]);
      const lastIndex = key.lastIndexOf('_');
      const n = _.toNumber(key.substr(lastIndex + 1, key.length));
      r[`${key.substr(0, idx)}.${cls}`] = n;
    }
  });

  return _.isEmpty(r) ? null : r;
}

export function classify(data) {
  const waveMap = {};
  _.forEach(data, (values, key) => {
    const w = classifyAndGroupWavelengths(key);
    if (w) {
      waveMap[key] = w;
    }
  });

  console.log(waveMap);
  const mapped = [];
  return _.chain(data)
    .map((values, key) => {
      if (_.includes(_.keys(waveMap), key)) {
        const measurement = Object.keys(waveMap[key])[0];
        if (_.includes(mapped, measurement)) {
          console.log(`igonoring ${key}`);
          return null;
        }
        mapped[measurement] = true;
        return 'wavelength';
      }

      let geoLocation = true;
      let number = true;

      let isSomething = false;

      _.forEach(values, (v) => {
        if (!v) {
          return;
        }

        if (!v.trim()) {
          return;
        }

        isSomething = true;

        const iv = _.toNumber(v);

        if (_.isNaN(iv)) {
          number = false;
        }

        const geo = v.split(' ');
        if (geo.length !== 4) {
          geoLocation = false;
        }

        const lat = _.toNumber(geo[0]);
        const lon = _.toNumber(geo[1]);

        if (_.isNaN(lat) || _.isNaN(lon)) {
          geoLocation = false;
        }

        if (lat < -90 || lat > 90) {
          geoLocation = false;
        }

        if (lon < -180 || lon > 180) {
          geoLocation = false;
        }
      });

      if (!isSomething) {
        return 'string';
      }
      if (geoLocation) {
        return 'geolocaton';
      }
      if (number) {
        return 'number';
      }
      return 'string';
    })
    .filter(a => a !== null)
    .value();
}

function resolveFieldRef(element, texts) {
  let resolved;
  try {
    const ref = $(element).attr('ref');
    const res = /jr:itext\('(\/data\/.*)+'/g.exec(ref);
    const extracted = texts[res[1]];
    resolved = extracted;
  } catch (error) {
    // console.error(error);
  }
  return resolved;
}

function extractId(element) {
  let extracted;
  try {
    const ref = $(element).attr('ref');
    const res = /jr:itext\('(\/data\/.*)+:label'/g.exec(ref);
    extracted = res[1];
  } catch (error) {
    // console.error(error);
  }
  return extracted;
}

function formItem(child, texts) {
  const label = resolveFieldRef($('label', child).get(0), texts);
  const hint = resolveFieldRef($('hint', child).get(0), texts);
  const childItems = $('item', child);

  const items = _.map(childItems, (e) => {
    const lbl = resolveFieldRef($('label', e).get(0), texts);
    const value = $('value', e).text();

    return {
      label: lbl,
      value,
    };
  });

  return {
    id: $(child).attr('ref'),
    label: label || '',
    hint: hint || '',
    items,
  };
}

function extractMeasurementKeys(prefixedId, survey) {
  const id = prefixedId.substring('/data/'.length, prefixedId.length);

  const prefix = `${id}.data.`;
  const exported = _.pickBy(survey, (values, question) => question.startsWith(prefix));
  return exported;
}

function classifyMeasurementRow(values) {
  let number = true;

  let isSomething = false;

  _.forEach(values, (v) => {
    if (!v) {
      return;
    }

    if (!v.trim()) {
      return;
    }

    isSomething = true;

    const iv = _.toNumber(v);

    if (_.isNaN(iv)) {
      number = false;
    }
  });

  if (!isSomething) {
    return 'string';
  }

  if (number) {
    return 'decimal';
  }
  return 'string';
}

function populateMeasurementExports(parent, items, survey) {
  const extracted = extractMeasurementKeys(parent.id, survey);
  const id = parent.id.substring('/data/'.length, parent.id.length);
  const prefix = `${id}.data.`;

  const mapped = {};
  const waveMap = {};
  _.forEach(extracted, (values, key) => {
    const w = classifyAndGroupWavelengths(key);
    if (w) {
      waveMap[key] = w;
    }
  });

  _.forEach(extracted, (v, question) => {
    if (_.includes(_.keys(waveMap), question)) {
      const m = Object.keys(waveMap[question])[0];
      if (_.includes(_.keys(mapped), m)) {
        return;
      }
      mapped[m] = true;
      const measurement = _.clone(parent);
      measurement.id = `/data/${m}`;
      measurement.label = `${parent.label} (${m})`;
      measurement.type = 'wavelength';
      const tmp = _.chain(waveMap)
        .map((wv, wk) => {
          if (_.keys(wv)[0] === m) {
            const res = {};
            res[wk] = wv[m];
            return res;
          }
          return null;
        })
        .filter(e => e !== null)
        .value();

      measurement.wavelengths = _.assign(...tmp);

      items.push(measurement);
      return;
    }

    const measurement = _.clone(parent);
    measurement.id = `/data/${question}`;
    measurement.label = `${parent.label} (${question.substr(prefix.length)})`;
    measurement.type = classifyMeasurementRow(v);
    items.push(measurement);
  });
}

export async function getFormDef(surveyId, survey) {
  const URL = `https://app.our-sci.net/api/survey/download/xml/by-form-id/${surveyId}`;
  const data = await fetch(URL);
  const t = await data.text();
  const parsed = $.parseXML(t);

  const structure = {};
  const texts = {};
  console.log(parsed);
  window.survey = parsed;

  $('h\\:head model itext translation:first', parsed)
    .children()
    .each((e, f) => {
      texts[$(f).attr('id')] = $('value', f).text();
    });

  $('h\\:head model bind', parsed).each((e, f) => {
    structure[$(f).attr('nodeset')] = {
      type: $(f).attr('type'),
    };
  });

  console.log(structure);
  console.log(texts);

  const items = [];
  const groups = {};

  $('h\\:body', parsed)
    .children()
    .each((e, f) => {
      if (f.nodeName === 'group') {
        const id = extractId($('label', $(f)).get(0));
        const hintId = extractId($('hint', $(f)).get(0));

        let label = texts[`${id}:label`];
        const hint = texts[`${hintId}:hint`];

        if (label && label.endsWith(';anon')) {
          label = label.substr(0, label.length - ';anon'.length);
          label += ' 👁️';
        }

        groups[id] = {
          label: label || '',
          hint: hint || '',
        };

        $(f)
          .children()
          .each((i, k) => {
            if (k.nodeName === 'label' || k.nodeName === 'hint') {
              return;
            }
            const r = formItem($(k), texts);
            r.group = id;

            if (structure[r.id].type === 'binary') {
              populateMeasurementExports(r, items, survey);
            } else {
              items.push(r);
            }
          });
      } else {
        const item = formItem($(f), texts);
        if (structure[item.id].type === 'binary') {
          populateMeasurementExports(item, items, survey);
        } else {
          items.push(item);
        }
      }
    });

  _.forEach(items, (item, idx) => {
    if (structure[item.id] && !item.type) {
      items[idx].type = structure[item.id].type;
    } else {
      // items[idx].type = 'string'; // todo figure out type
    }
    items[idx].id = items[idx].id.substring('/data/'.length);
  });

  return {
    items,
    groups,
  };

  // $('')
  /// jr:itext\('(\/data\/.*)+'/g.exec()
}

export function map(pins) {
  const container = document.getElementById('container');
}

export function waveLengthPlot(title, yaxisLabel, survey, keys, element) {
  const validSet = hasAtLeastOneNumber(survey, keys);
  const trace = tracesFromKeys(validSet, keys, 'Sample_ID');
  const p = linecharts(_.keys(keys), trace.traces, trace.names, ['wavelength [nm]', yaxisLabel]);

  dashboard.plotly(
    `${title} (n = ${trace.traces.length})`,
    p.data,
    p.layout,
    p.config,
    true,
    element,
  );
}
