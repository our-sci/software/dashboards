// Type definitions for src/dashboard.js
// Project: [LIBRARY_URL_HERE] 
// Definitions by: [YOUR_NAME_HERE] <[YOUR_URL_HERE]> 
// Definitions: https://github.com/borisyankov/DefinitelyTyped
declare namespace dashboard{
	// dashboard.plotly.!3
	
	/**
	 * 
	 */
	interface Plotly3 {
				
		/**
		 * 
		 */
		responsive : boolean;
	}
}

/**
 * 
 */
declare namespace dashboard{
		
	/**
	 * 
	 */
	function sayHello(): void;
		
	/**
	 * 
	 * @param title 
	 * @param description 
	 * @param icon 
	 */
	function info(title : any, description : any, icon : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param content 
	 * @param progress 
	 * @param progressText 
	 * @param icon 
	 */
	function progressBox(title : any, content : any, progress : any, progressText : any, icon : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param content 
	 * @param action 
	 * @param icon 
	 * @param actionIcon 
	 * @param callback 
	 */
	function actionBox(title : any, content : any, action : any, icon : any, actionIcon : any, callback : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param wide 
	 */
	function genericBox(title : any, wide : any): void;
		
	/**
	 * 
	 * @param surveys 
	 */
	function fetchSurveys(surveys : any): void;
		
	/**
	 * 
	 * @param primary 
	 * @param secondary 
	 * @param accent 
	 */
	function style(primary : any, secondary : any, accent : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param data 
	 */
	function debug(title : any, data : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param x 
	 * @param values 
	 * @param axis 
	 * @param wide 
	 */
	function lineChart(title : any, x : any, values : any, axis : any, wide : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param data 
	 * @param layout 
	 * @param config 
	 * @param wide 
	 * @param element 
	 */
	function plotly(title : any, data : any, layout : any, config : dashboard.Plotly3, wide : any, element : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param data 
	 */
	function csvDownloadBox(title : any, data : any): void;
		
	/**
	 * 
	 * @param title 
	 * @param data 
	 * @param callback 
	 * @param wide 
	 * @param element 
	 */
	function selectBox(title : any, data : any, callback : any, wide : any, element : any): void;
		
	/**
	 * 
	 * @param tableData 
	 */
	function table(tableData : any): void;
		
	/**
	 * 
	 * @param title 
	 */
	function section(title : any): void;
		
	/**
	 * 
	 */
	function showLoader(): void;
		
	/**
	 * 
	 */
	function hideLoader(): void;
		
	/**
	 * 
	 * @param baseSurveyParam 
	 * @param baseSurveyJoinField 
	 * @param surveyToJoin 
	 * @param surveyToJoinField 
	 * @param prefix 
	 */
	function joinSurveyOn(baseSurveyParam : any, baseSurveyJoinField : any, surveyToJoin : any, surveyToJoinField : any, prefix : any): void;
		
	/**
	 * 
	 * @param surveysParam 
	 */
	function mergeSurveys(surveysParam : any): void;
}
