module.exports = {
  extends: 'airbnb-base',
  rules: {
    'no-console': 0,
    'no-mixed-operators': 0,
    'spaced-comment': [2, 'always', { exceptions: ['-'], markers: ['/'] }],
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'import/no-cycle': 0,
    'no-plusplus': 0,
    'prefer-destructuring': 0,
  },
  env: {
    browser: 'true',
  },
  globals: {
    dashboard: false,
    _: false,
    $: false,
  },
};
